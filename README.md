# Projet analyse data - M2 SSIO 


## Auteurs : 
 * Djamila HOCINE
 * Omar ERRIAHI EL IDRISSI 
 * Hamza JERANDO 
 * Banou-Priya SINNATAMBY


## Ce projet contient :
 - le repertoire "got_project" code du shinyApp
 - le rapport "Rapport - Analyse data R - Omar ERRIAHI EL IDRISSI - Banou-Priya SINNATAMBY - Hamza JERANDO - Djamila HOCINE"   
 - le fichier "README"

